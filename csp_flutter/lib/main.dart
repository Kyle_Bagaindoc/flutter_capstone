import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/screens/login_screen.dart';
import '/screens/project_list_screen.dart';
import '/providers/user_provider.dart';

Future<void> main() async {
      
    await dotenv.load(fileName: '.env');    
    WidgetsFlutterBinding.ensureInitialized();
    
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? accessToken = prefs.getString('accessToken');
    String? designation = prefs.getString('designation');
    String initialRoute = (accessToken != null) ? '/project-list' : '/';

    runApp(App(initialRoute, accessToken, designation));
}

class App extends StatelessWidget {
    final String _initialRoute;
    final String? _accessToken;
    final String? _designation;

    App(this._initialRoute, this._accessToken, this._designation);

    @override
    Widget build(BuildContext context) {

        return ChangeNotifierProvider(
            create: (BuildContext context) => UserProvider(_accessToken, _designation),
            child: MaterialApp(  theme:ThemeData(
                brightness: Brightness.light,
               
                elevatedButtonTheme: ElevatedButtonThemeData(
                    style: ElevatedButton.styleFrom(
                                     
                    )
                )

            ),
                initialRoute: _initialRoute,
                routes: {
                    '/': (context) => LoginScreen(),
                    '/project-list': (context) => ProjectScreen(),
                    
                }

            )
        );
    }   
}
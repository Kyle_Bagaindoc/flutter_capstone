import 'package:csp_flutter/utils/api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/utils/functions.dart';
import '/utils/themes.dart';
import '/providers/user_provider.dart';

class AddProjectDialog extends StatefulWidget {
    @override
    _AddProjectDialog createState() => _AddProjectDialog();
}

class _AddProjectDialog extends State<AddProjectDialog> {
    final _formKey = GlobalKey<FormState>();
    final _txtNameController = TextEditingController();
    final _txtDescriptionController = TextEditingController();

    @override
    Widget build(BuildContext context) {
        final FocusScopeNode focusNode = FocusScope.of(context);
        final String? accessToken = Provider.of<UserProvider>(context).accessToken;
            
            API(accessToken).addProject(
            description: _txtDescriptionController.text, name: ''
        ).catchError((error) {
            showSnackBar(context, error.message);
        });



        Widget txtName = TextFormField(
            decoration: InputDecoration(labelText: 'Name'),
            keyboardType: TextInputType.text,
            controller: _txtNameController,
            onEditingComplete: focusNode.nextFocus,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'Name is required.';
            }
        );

        Widget txtDescription = TextFormField(
            decoration: InputDecoration(labelText: 'Description'),
            keyboardType: TextInputType.text,
            controller: _txtDescriptionController,
            onEditingComplete: focusNode.nextFocus,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'Description is required.';
            }
        );

        Widget formAddProject = Form(
            key: _formKey,
            child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                    txtName,
                    txtDescription
                ]
            )
        );

        return AlertDialog(
            title: Text('Add New Project'),
            content: Container(
                child: formAddProject
            ),
            actions: [
                ElevatedButton(
                    child: Text('Add'),
                    onPressed: () {
                        if (_formKey.currentState!.validate()) {
                            Navigator.of(context).pop();
                        } else {
                            showSnackBar(context, 'Form validation failed. Check input and try again.');
                        }
                    },
                ),
                ElevatedButton(
                    child: Text('Cancel'),
                    onPressed: () {
                        Navigator.of(context).pop();
                    }
                )
            ]
        );
    }
}
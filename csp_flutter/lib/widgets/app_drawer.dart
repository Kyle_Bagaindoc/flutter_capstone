import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';


import '/providers/user_provider.dart';

class AppDrawer extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        final Function setAccessToken = Provider.of<UserProvider>(context, listen: false).setAccessToken;
        final Function setDesignation = Provider.of<UserProvider>(context, listen: false).setDesignation;

        return Drawer(
            child: Container(
                width: double.infinity,
                color: Color.fromRGBO(20, 45, 68, 1),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        DrawerHeader(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                
                                    SizedBox(height: 5.0, width: double.infinity), 
                                    Image.asset('assets/ffuf-logo.png',
                                    width: 180, color: Colors.white,), 
                                    Container(
                                       margin: EdgeInsets.only(top: 10),
                                
                                        child: Column(
                                            children: [
                                                
                                                Text('FFUF Project Management', style: TextStyle(fontSize: 20.0, color: Colors.white)),
                                                Text('Made by FFUF Internal Dev Team', style: TextStyle(color: Colors.white)),
                                            ]
                                        )
                                    )
                                ]
                            )
                        ),
                        Spacer(),
                      
                        Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [ListTile(
                                    title: Text('About', style: TextStyle(color: Colors.white)),
                                    onTap: ()  {

                                            // Navigator.push(context, MaterialPageRoute(builder: (context) => AboutScreen()));             
                                   
                                    }
                                ),
                                ListTile(
                                    title: Text('Logout', style: TextStyle(color: Colors.white)),
                                    onTap: () async {

                                Provider.of<UserProvider>(context, listen: false).setAccessToken(null);
                                Provider.of<UserProvider>(context, listen: false).setDesignation(null);
                                
                                SharedPreferences prefs = await SharedPreferences.getInstance();

                                prefs.remove('accessToken');
                                prefs.remove('designation');

                                Navigator.pushNamedAndRemoveUntil(context, '/', (Route<dynamic> route) => false);

                                    }
                                )
                            ]
                        )
                    ]
                )
            )
        );
    }
}
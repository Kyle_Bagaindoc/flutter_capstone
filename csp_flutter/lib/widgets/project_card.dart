import 'package:csp_flutter/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/models/project.dart';
import '/providers/user_provider.dart';
import '/screens/task_list_screen.dart';
import '/widgets/assign_project_dialog.dart';


class ProjectCard extends StatefulWidget {
    final Project _project;
    final Function _reloadProjects;

    ProjectCard(this._project, this._reloadProjects);

    @override
    _ProjectCard createState() => _ProjectCard();
}

class _ProjectCard extends State<ProjectCard> {    
    @override
    Widget build(BuildContext context) {
        final String? designation = Provider.of<UserProvider>(context).designation;

        Widget ltProjectInfo = ListTile(
            title: Text(widget._project.name!),
            subtitle: Wrap(
                direction: Axis.vertical, 
                children: [
                    Text(widget._project.description!)
                ]
            )
        );

        Widget btnAssign = Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            child: ElevatedButton(
                child: Text('Assign', style: TextStyle(fontWeight: FontWeight.bold),),
                onPressed: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) => AssignProjectDialog(widget._project.id)
                    ).then((value) {
                        widget._reloadProjects();
                    });
                },
                  style: btnDefaultTheme
            )
        );

        Container btnTasks =  Container( 
            
            margin: EdgeInsets.symmetric(horizontal: 4),
            child:  ElevatedButton( 
                style: ElevatedButton.styleFrom(primary: Colors.red),
                child: Text('Tasks'),
                onPressed: () async {
                    await Navigator.push(context, MaterialPageRoute(builder: (context) => TaskList(widget._project.id)));
                    widget._reloadProjects();
                },
              
            )
        );

        Row rowActions = Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
               (widget._project.assignedTo == null && designation =='contractor') ? btnAssign : Container(), btnTasks
                
                
            
            ]
        );
        
        return Card(
            child: Padding(
                padding: EdgeInsets.all(16.0), 
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                        ltProjectInfo,
                        rowActions
                    ]
                )
            )
        );
    }
}
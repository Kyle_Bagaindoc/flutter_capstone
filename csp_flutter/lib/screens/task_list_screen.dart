import 'package:csp_flutter/widgets/add_project_dialog.dart';
import 'package:csp_flutter/utils/api.dart';
import 'package:csp_flutter/utils/functions.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/models/task.dart';
import '/providers/user_provider.dart';
import '/widgets/task_card.dart';

class TaskList extends StatefulWidget {
    final int? _projectId;

    TaskList(this._projectId);

    @override
    _TaskList createState() => _TaskList();
}

class _TaskList extends State<TaskList> {
    Future<List<Task>>? _futureTasks;

    final _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

    void _reloadTasks() {
            final String? accessToken = context.read<UserProvider>().accessToken;
            setState(() {
                _futureTasks = API(accessToken).getTasks(widget._projectId).catchError((error) {
                    showSnackBar(context, error.message);
                });
            });
    }

    Widget _showTasks(List? tasks) {
        var cardTasks = tasks!.map((task) => TaskCard(task, _reloadTasks)).toList();

        return RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () async {
                _reloadTasks();
            },
            child: ListView(
                children: cardTasks
            ), 
        );
    }

    @override
    void initState() {
        super.initState();

        WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
            final String? accessToken = context.read<UserProvider>().accessToken;
            setState(() {
                _futureTasks= API(accessToken).getTasks(widget._projectId).catchError((error) {
                    showSnackBar(context, error.message);
                });
            });
        });
    }

    @override
    Widget build(BuildContext context) {
        final String? designation = Provider.of<UserProvider>(context).designation;

        Widget addTask = FloatingActionButton(
            child: Icon(Icons.add),
            backgroundColor: Color.fromRGBO(205, 23, 25, 1),
            foregroundColor: Colors.white,
            onPressed: () {
                    showDialog(
                    context: context,
                    builder: (BuildContext context) => AddProjectDialog()
                ).then((value) {{
                     final String? accessToken = Provider.of<UserProvider>(context, listen: false).accessToken;

                    setState(() {
                    _futureTasks = API(accessToken).getTasks(widget._projectId).catchError((error) {
                    showSnackBar(context, error.message);
                });
            });
        }

        });
            },
        );

        Widget taskListView = FutureBuilder(
            future: _futureTasks,
            builder: (context, snapshot) {
                if (snapshot.hasData) {
                    return Container(
                        child: _showTasks(snapshot.data as List)
                    );
                } else {
                    return Center (
                        child: CircularProgressIndicator()
                    );
                }
            }
        );

        return Scaffold(
            appBar: AppBar(title: Text('Project Task List')),
            body: Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: taskListView
            ),
            floatingActionButton: (designation == 'subcontractor') ? addTask : null,
        );
    }
}
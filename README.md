# Flutter Capstone

# Versions

version: 1.0.0+1

## Packages

- [**sdk: ">=2.12.0 <3.0.0"**](https://flutter.dev/docs/development/tools/sdk)
    - Software Development Kit or SDK it has the packages and command-line tools that you need to develop Flutter apps across platforms. To get the Flutter SDK
- [**http: ^0.13.3**](https://flutter.dev/docs/cookbook/networking/fetch-data)
    - This package contains a set of high-level functions and classes that make it easy to consume HTTP resources. It's multi-platform, and supports mobile, desktop, and the browser.
- **provider: ^5.0.0**
    - a common way to consume these InheritedWidgets
- [**shared_preferences: ^2.0.6**](https://pub.dev/packages/shared_preferences)
    - Wraps platform-specific persistent storage for simple data (NSUserDefaults on iOS and macOS, SharedPreferences on Android, etc.).
- [**image_picker: ^0.8.0+3**](https://pub.dev/packages/image_picker)
    - A Flutter plugin for iOS and Android for picking images from the image library, and taking new pictures with the camera.
- **flutter_dotenv: ^5.0.0**
    - Load configuration at runtime from a .env file which can be used throughout the application
- **cupertino_icons: ^1.0.2**
    - This is an asset repo containing the default set of icon assets used by Flutter's Cupertino widgets.
- [**google_fonts: ^2.1.0**](https://pub.dev/packages/google_fonts)
    - The google_fonts package for Flutter allows you to easily use any of the 977 fonts (and their variants).
- **sdk: flutter**
    - The Flutter SDK has the packages and command-line tools that you need to develop Flutter apps across platforms.
- [**uses-material-design: true**](https://material.io/develop/flutter)
    - Build beautiful, usable products using Material Components for Flutter, a mobile UI framework
- assets:

    [- assets](https://flutter.dev/docs/development/ui/assets-and-images)

    An asset is a file that is bundled and deployed with your app, and is accessible at runtime.

    [**- .env**](https://pub.dev/packages/flutter_dotenv)

    An environment is the set of variables known to a process (say, PATH, PORT, ...). It is desirable to mimic the production environment during development (testing, staging, ...) by reading these values from a file.

# Features

The features of the Flutter Project are as follows:

- Contractor

    - The contractor can login and will be directed to Project List Screen where he/she can show all projects, add projects, assign a subcontractor to the project and review finished project tasks.

- Subcontractor

    - The subcontractor can show the assigned projects, show the project tasks, add and assign them to the assembly team.

- Assembly Team
    - The assembly team can show the projects with the assigned tasks, , show the project tasks, tag the task as ongoing, pending or completed

# Setup Guide

 

- Cloning the project locally:
    1. 
    2. Open Terminal in your computer.
    3.  Change the current working directory to the location here you want the clone directory.
    4. Type `git clone` ,and then paste the url of your bitbucket . `git clone <url>`.
    5.  Go to your flutter project by using the command: `cd <your flutter_app_name>`
    6. Type  `flutter run` to start running your cloned flutter app.

    Another option would be: 

    1. Open VScode and take a new window.
    2. Press " ctrl+shift+p " for making command pallet to display.
    3. Type in Git in the pallet on top. 
    4. Select the suggested Git: clone option.
    5. Paste the Git URL in the pallet of the project you have cloned.

- Installing the flutter packages
    1. Depend on it

        Open the `pubspec.yaml` file located inside the app folder and add the packages you need.

    2. Install it
        - From the terminal: Run `flutter pub get`.
        - From VS Code: Click **Get Packages** located in right side of the action ribbon at the top of `pubspec.yaml`.

# User Credentials

---

user: contractor@gmail.com

password: contractor

---

user: subcontractor@gmail.com

password: subcontractor

---

user: assemblyteam@gmail.com

password: assemblyteam

---

# Images of the App

## Login Page

![capstone login.png](Flutter%20Capstone%20738c834da7864d2e8d3b48755e441815/capstone_login.png)

![capstone homepage.png](Flutter%20Capstone%20738c834da7864d2e8d3b48755e441815/capstone_homepage.png)

## Homepage

![capstone homepage.png](Flutter%20Capstone%20738c834da7864d2e8d3b48755e441815/capstone_homepage%201.png)

## Drawer

![capstone drawer.png](Flutter%20Capstone%20738c834da7864d2e8d3b48755e441815/capstone_drawer.png)